﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DiffusedaBomb
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class ClassButtons
    {
        public void Invisibility(){all_button.IsVisible = visibility;}
        public Button all_button { get; set; }
        public Type type { get; set; }
        public string Text { get; set; }
        public bool visibility { get; set; }
        public string value { get; set; }
       
    }
    public enum Type
    {
        Bomb,
        DiffuseR
    }

    public partial class Startgame : ContentPage
	{
      
        int levels = 1;
        int diffused = 0;
        int points = 50;

        List<ClassButtons> List_buttons;
        public Startgame ()
		{
           
            InitializeComponent ();
            
            txtLevels.Text = levels.ToString();
            txtPoints.Text = points.ToString();
        }
      
        protected override void OnAppearing()//for appearing the buttons
        {
          
            base.OnAppearing();
            diffused = 0;

            viewButtons();

            Random random = new Random();
           
            for(int i = 0; i < 2; i++)//for randoming the diffused value on the button
            {
                int number = random.Next(1, 5);
                if(List_buttons[number].type == Type.DiffuseR)
                {
                    i--;continue;
                }
                List_buttons[number].type = Type.DiffuseR;
                List_buttons[number].value = "Diffuser";
               
                foreach (var button in List_buttons)
                {
                    button.visibility = true;
                    button.Invisibility();
                    button.all_button.IsEnabled = true;
                    button.all_button.Text = button.Text;
                    
                }

            }
        }
        async private void Bombbutton_OnClicked(object sender, EventArgs e)
        {
            for (int x = 0; x < 5; x++)
            {
                if (sender == List_buttons[x].all_button)
                {
                    List_buttons[x].all_button.IsEnabled = false;
                    List_buttons[x].all_button.Text = List_buttons[x].value;

                    if (List_buttons[x].type == Type.Bomb)
                    {
                        
                        await DisplayAlert("Oooops! There's a Bomb!", "Points: -20", "Ok");
                        await DisplayAlert("Revealed Answers: ","Button1 == "+List_buttons[0].value + "\nButton2 ==  " + List_buttons[1].value + "\nButton3 == " + List_buttons[2].value + "\nButton4 == " + List_buttons[3].value + "\nButton5 == " + List_buttons[4].value,"OK");
                        levels++;
                        txtLevels.Text = levels.ToString();
                        points = points - 20;
                        txtPoints.Text = points.ToString();
                        OnAppearing();//move to next level
                        if (points <=0)
                        {
                            points = 0;
                            txtPoints.Text = points.ToString();
                            var answer = await DisplayAlert("Yay! You Lose!", "Game Over. Do you want to continue Playing?", "Yes", "No");
                            if (answer == true)
                            {
                                levels = 1;
                                points = 50;
                                txtLevels.Text = levels.ToString();
                                txtPoints.Text = points.ToString();
                                OnAppearing();
                            }
                            else
                            {
                                await Navigation.PopAsync();
                            }
                        }
                    }
                    else
                    {
                        diffused++;
                        await DisplayAlert("Grats! You found a diffuser to the Bomb!", "Points: +10", "Ok");
                        points = points + 10;
                        txtPoints.Text = points.ToString();
                        txtLevels.Text = levels.ToString();
                        if (diffused == 2)
                        {
                            await DisplayAlert("Congratulations! ", "You WIN in this round!", "Ok");
                            levels++;
                            txtLevels.Text = levels.ToString();
                            OnAppearing();//move to next level
                        }
                    }
                }
            }
        }
        async void Restart_OnClicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Restart the Game?", "Are you sure you want restart?", "Yes", "No");
            if (answer==true)
            {   
                levels = 1;
                points = 50;
                txtLevels.Text = levels.ToString();
                txtPoints.Text = points.ToString();
                OnAppearing();
            }
            else
            {
                return;
            }
        }
        async void End_OnClicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Quit?", "Are you sure you want to quit?", "Yes", "No");
            if (answer == true)
            {
                await Navigation.PopAsync();
            }
            else
            {
                return;
            }
        }
        async void Skip_OnClicked(object sender, EventArgs e)
        {
            if (points < 31)
            {
                await DisplayAlert("Warning!", "You dont have enough points. You must have more than 30pts in order to skip.", "Ok");
                return;
            }
            var answer = await DisplayAlert("Warning!", "Are you sure you want to skip this level? 30pts will be deducted to your points.", "Yes", "No");
            if (answer == true)
            {
                levels = levels + 1;
                points = points - 30;
                txtLevels.Text = levels.ToString();
                txtPoints.Text = points.ToString();
                OnAppearing();
               
            }
            else
            {
                return;
            }
        }
        public void viewButtons()
        {
            List_buttons = new List<ClassButtons>();

            List_buttons = new List<ClassButtons>
            {
                new ClassButtons{ value = "Bomb", Text="1",all_button=Bomb1, type=Type.Bomb},
                new ClassButtons{value="Bomb", Text="2",all_button=Bomb2, type=Type.Bomb},
                new ClassButtons{value = "Bomb", Text = "3",all_button =Bomb3, type=Type.Bomb},
                new ClassButtons{ value="Bomb", Text="4",all_button=Bomb4, type=Type.Bomb},
                new ClassButtons{ value="Bomb", Text="5",all_button=Bomb5, type=Type.Bomb}
            };
        }

    }
}
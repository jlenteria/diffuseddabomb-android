﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DiffusedaBomb
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async private void Startgame_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Startgame());
        }
        async public void Exit_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Exit the Game?", "Do you want to Exit the game?", "Yes", "No");
            if (answer == true)
            {
                System.Environment.Exit(1);
            }
            else
            {
                return;
            }

        }


        async private void About_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new About());
        }

       

        async private void Help_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Help());
        }
    }
}
